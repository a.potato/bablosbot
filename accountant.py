#!/usr/bin/python
# coding=utf-8
import traceback

import telegram
import random
import logging
from data import data_model as dm
from sqlalchemy import exists

logger = logging.getLogger('bot.accountant')

def eval_string_expression(string_exp):
    import re
    pattern = r'[^0-9/*+-.,\(\)]'
    if re.search(pattern, string_exp):
        logger.debug("eval_string unexpected symbol: '%s'" % string_exp)
        return None
    else:
        try:
            value = eval(string_exp)
        except:
            logger.debug("eval_string unexpected symbol: '%s'" % string_exp)
            return None
        value = round(value, 2)
        return value

class Accountant(object):
    def __init__(self, log, db_session):
        self._db = db_session
        self._groups = dict()
        self._group_id_counter = 0

        self._log = log
        self.db_grouplist = "grouplist"

        self._reserved_names = ["все", "стоп", "stop", "отмена", "cancel"]
        self.logger = logger

        # for assoc in self._db.query(dm.AssociationUserGroups).all():
        #    assoc.hidden = False
        # self._db.commit()

    def create_group(self, group_name):
        gid = self._add_group(group_name)
        return gid

    def _add_group(self, group_name):
        gid = self._create_group_id()
        invite = random.randint(100000, 999999)
        while invite in self._db.query(dm.Group.invite_code).all():
            invite = random.randint(100000, 999999)

        self.logger.debug('group created: gid=%d, invite=%d' % (gid, invite))

        group = dm.Group(name=group_name, invite_code=invite, gid=gid)
        self._db.add(group)
        self._db.commit()

        return gid

    def add_user_to_group(self, uid, gid, name):
        group = self._db.query(dm.Group).filter_by(gid=gid).one()
        member = dm.Member(name=name)
        member.group = group
        assoc = dm.AssocUserMember()
        assoc.member = member
        self.logger.debug('group (gid=%d): new member (name=%s) ' %
                          (gid, name))

        if uid:
            user = self._db.query(dm.User).filter_by(telegram_uid=uid).one()
            assoc.user = user
            self.logger.debug('group (gid=%d): member-user associated (name=%s, telegram_uid=%d)' %
                              (gid, name, uid))

        self._db.commit()

    def _create_group_id(self):
        used_gids = self._db.query(dm.Group.gid).all()
        biggest = max(used_gids)
        gid = biggest[0] + 1
        self.logger.debug('group id generated: %d' % gid)
        return gid

    def create_payment_for_group(self, group_id, payment_name, member_ids, amounts, rates):
        if len(member_ids) != len(amounts) or len(member_ids) != len(rates):
            raise Exception("expected input of same length")

        group = self._db.query(dm.Group).filter_by(gid=group_id).one()
        transaction = dm.Transaction(name=payment_name)
        for member_id, amount, rate in zip(member_ids, amounts, rates):
            payment = dm.Payment(amount=amount, rate=rate)
            member = self._db.query(dm.Member) \
                .filter(dm.Member.id == member_id) \
                .one()
            payment.member = member
            transaction.payments.append(payment)
        group.transactions.append(transaction)
        self._db.commit()
        return transaction

    def get_group_ids_by_uid_hidden(self, telegram_uid):
        results = self._db.query(dm.Group.gid) \
            .join(dm.Member) \
            .join(dm.AssocUserMember) \
            .join(dm.User) \
            .filter(dm.User.telegram_uid == telegram_uid) \
            .filter(dm.Member.archived.is_(True)).all()
        gids = [result[0] for result in results]

        if len(gids) >= 1:
            return gids
        else:
            return None

    def get_group_ids_by_uid_visible(self, telegram_uid):
        results = self._db.query(dm.Group.gid) \
            .join(dm.Member) \
            .join(dm.AssocUserMember) \
            .join(dm.User) \
            .filter(dm.User.telegram_uid == telegram_uid) \
            .filter(dm.Member.archived.is_(False)).all()
        gids = [result[0] for result in results]

        if len(gids) >= 1:
            return gids
        else:
            return None

    def get_group_name(self, gid):
        group = self._db.query(dm.Group) \
            .filter_by(gid=gid).one()
        return group.name

    def get_members_from_group(self, gid):
        result = self._db.query(dm.Member) \
            .join(dm.Group) \
            .filter(dm.Group.gid == gid) \
            .all()

        return result

    def get_uids_from_group(self, gid):
        result = self._db.query(dm.User.telegram_uid) \
            .join(dm.AssocUserMember) \
            .join(dm.Member) \
            .join(dm.Group) \
            .filter(dm.Group.gid == gid) \
            .all()
        # [(182116723,), (100806533,), (410903251,), (211849869,), (299268874,)]
        uids = [res[0] for res in result]
        return uids

    def get_names_from_group(self, gid):
        query = self._db.query(dm.Member.name) \
            .join(dm.Group) \
            .filter(dm.Group.gid == gid).all()
        names = [name[0] for name in query]
        return names

    def get_name_by_uid_from_group(self, gid, telegram_uid):
        username = self._db.query(dm.Member.name) \
            .join(dm.AssocUserMember) \
            .join(dm.User) \
            .join(dm.Group) \
            .filter(dm.User.telegram_uid == telegram_uid) \
            .filter(dm.Group.gid == gid) \
            .one()
        # group = self._db.query(dm.Group).filter_by(gid=gid).one()
        # names = [user_assoc.user.name for user_assoc in group.users if user_assoc.user.telegram_uid == uid]
        # assert len(names) == 1, "Query returned invalid result"

        return username[0]

    def get_member_id_from_user(self, telegram_uid, gid):
        member = self._member(telegram_uid, gid)
        return member.id

    def _member(self, telegram_uid, gid):
        member = self._db.query(dm.Member) \
            .join(dm.AssocUserMember) \
            .join(dm.User) \
            .join(dm.Group) \
            .filter(dm.User.telegram_uid == telegram_uid) \
            .filter(dm.Group.gid == gid) \
            .one()

        return member

    def get_telegram_uid_from_member(self, member_id):
        telegram_uid = self._db.query(dm.User.telegram_uid) \
            .join(dm.AssocUserMember) \
            .filter(dm.AssocUserMember.member_id == member_id) \
            .one_or_none()

        return telegram_uid[0] if telegram_uid else telegram_uid

    def get_member_name(self, telegram_uid=None, gid=None, member_id=None):
        if member_id:
            member = self._db.query(dm.Member).get(member_id)
        elif telegram_uid and gid:
            member = self._member(telegram_uid, gid)
        else:
            raise ValueError("should provide either 'member_id' or 'telegram_uid + gid'")

        return member.name

    def calc_balances(self, gid):
        group = self._db.query(dm.Group).filter_by(gid=gid).one()
        balances = dict()
        for member in group.members:
            balances[member.id] = 0
        for trans in group.transactions:
            trans_credits = self.calc_transaction_credits(trans)
            for uid, credit in trans_credits.items():
                balances[uid] += credit

        return balances

    # Dictionary of [user_id] = amount
    # Amount of money is how much a person payed for this transaction
    # Minus how much of it he owes for
    def calc_transaction_credits(self, trans):
        credits = dict()

        trans_sum = 0.0
        rate_sum = 0.0
        for payment in trans.payments:
            trans_sum += payment.amount
            rate_sum += payment.rate
            credits[payment.member.id] = 0.0

        if rate_sum == 0:
            return credits

        amount_by_rate = trans_sum / rate_sum
        for payment in trans.payments:
            credits[payment.member.id] += payment.amount - amount_by_rate * payment.rate

        return credits

    def get_names_balances(self, gid):
        balances = self.calc_balances(gid)
        names_balances_tuples = []
        for uid, balance in balances.items():
            name = self._db.query(dm.Member.name) \
                .join(dm.Group) \
                .filter(dm.Member.id == uid) \
                .filter(dm.Group.gid == gid) \
                .one()
            names_balances_tuples.append((name[0], balance))

        return names_balances_tuples

    def transaction_string(self, transaction_id):
        transaction = self._db.query(dm.Transaction).get(transaction_id)
        # TransName: Name1 Sum1 Name2 Sum2 .. за Name1, Name2, Name3 (1:1:1)
        text = transaction.name + ":"
        for payment in transaction.payments:
            if payment.amount == 0:
                continue
            username = self._db.query(dm.Member.name) \
                .filter(dm.Member.id == payment.member_id) \
                .filter(dm.Member.group_id == transaction.group_id) \
                .one()
            text += " %s %g," % (username[0], payment.amount)
            # if payment != transaction.payments[-1]:
            #    text += ", "

        text += " за "
        debitors_query = self._db.query(dm.Member.name, dm.Payment.rate) \
            .join(dm.Payment) \
            .filter(dm.Payment.transaction == transaction) \
            .filter(dm.Payment.member_id == dm.Member.id) \
            .filter(dm.Payment.rate != 0) \
            .all()

        text += ', '.join([debitor[0] for debitor in debitors_query])
        for debitor in debitors_query:
            if debitor[1] != debitors_query[0][1]:
                # Если есть неодинаковый коэффициент - вывести все
                text += " (" + ':'.join(["%g" % local_debitor[1] for local_debitor in debitors_query]) + ")"
                break

        return text

    def show_last_transactions(self, gid, max_amount=0):
        transactions_id = self._db.query(dm.Transaction.id) \
                              .join(dm.Group) \
                              .filter(dm.Group.gid == gid)[-max_amount:]

        names = self.get_names_from_group(gid)

        text = "Участники: "
        text += ", ".join(names) + "\n"
        if len(transactions_id) == 0:
            text += "Чеков в группе пока нет"
        else:
            text += "Последние записи:"
            for trans_id in transactions_id:
                text += "\n" + self.transaction_string(trans_id[0])
        return text

    def transactions_from_group(self, gid):
        transactions = self._db.query(dm.Transaction) \
            .join(dm.Group) \
            .filter(dm.Group.gid == gid) \
            .all()
        return transactions

    def get_member_credit(self, member_id):
        # find member from group with user with tuid
        member = self._db.query(dm.Member) \
            .filter(dm.Member.id == member_id) \
            .one()

        gid = member.group.gid
        balances = self.calc_balances(gid)

        credit = balances[member.id]
        return credit

    def member_trans_credit(self, transaction, member_id):
        # return self._groups[group_id].user_trans_credit(transaction_id, uid)
        # transaction = self._db.query(dm.Transaction) \
        #     .filter_by(id=transaction_id) \
        #     .one()
        credits = self.calc_transaction_credits(transaction)
        credit = credits[member_id]
        return credit

    def find_gid_by_invite(self, invite_code):
        gid = self._db.query(dm.Group.gid).filter_by(invite_code=invite_code).one_or_none()
        return gid[0] if gid is not None else None

    def get_invite_by_gid(self, gid):
        invite = self._db.query(dm.Group.invite_code).filter_by(gid=gid).one()
        return invite[0]
        # return self._group_invites[gid]

    def remove_transaction(self, trans_id):
        self.logger.info('Removing transaction: trans_id=' + str(trans_id))
        trans = self._db.query(dm.Transaction).get(trans_id)
        self._db.delete(trans)
        self._db.commit()

    # def user_from_userid(self, userid_list):
    #     users = self._db.query(dm.User).filter(dm.User.id.in_(userid_list)).all()
    #     return users

    def set_group_archived(self, telegram_uid, gid, archived=True):
        member = self._db.query(dm.Member) \
            .join(dm.AssocUserMember) \
            .join(dm.User) \
            .join(dm.Group) \
            .filter(dm.User.telegram_uid == telegram_uid) \
            .filter(dm.Group.gid == gid) \
            .one()

        member.archived = archived
        self._db.commit()
