#!/usr/bin/python
# coding=utf-8
import traceback

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
import telegram
from telegram.error import TelegramError
from accountant import Accountant
from dialog_pay import DialogPay
from dialog_front import DialogFront
#from google_client import GsheetsController
import logging
from logging.handlers import RotatingFileHandler
import datetime
from reception import Reception
from data.data_model import create_session
import os
from io import StringIO
import json

NO_DB = False


class Bot(object):
    def __init__(self):

        self._db_session = create_session('sqlite:///database.sqlite')
        self._accountant = Accountant(self._log, self._db_session)
        self.load_config("config.json")

        token = self._config['telegram']['token']
        self._updater = Updater(token=token)
        dispatcher = self._updater.dispatcher

        os.makedirs("logs", exist_ok=True)
        log_filename = "logs/%s.log" % datetime.datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                            # datefmt='%Y-%m-%d %H:%M:%S',
                            level=logging.INFO,
                            handlers=[RotatingFileHandler(log_filename, mode='a', maxBytes=10*1024*1024,
                                                          backupCount=20, encoding='utf-8', delay=0)]
                                      #logging.StreamHandler()]
                            )
        # fh = logging.FileHandler(log_filename, 'w', 'utf-8')
        # fh.setLevel(logging.DEBUG)
        # ch = logging.StreamHandler()
        # ch.setLevel(logging.DEBUG)
        # fh.setFormatter(formatter)
        self.logger = logging.getLogger('bot')
        self.logger.setLevel(logging.DEBUG)
        #self.logger.addHandler(fh)
        # self.logger.addHandler(ch)
        self.logger.debug("hello world")

        self._bot = self._updater.bot
        self.reception = Reception(self._bot, self._accountant, self._log, self._db_session)

        start_handler = CommandHandler("start", self._start)
        echo_handler = MessageHandler(Filters.text, self._echo)
        query_handler = CallbackQueryHandler(self._handle_query)

        dispatcher.add_handler(start_handler)
        # dispatcher.add_handler(add_handler)
        dispatcher.add_handler(echo_handler)
        dispatcher.add_error_handler(self._handle_telegram_error)
        dispatcher.add_handler(query_handler)

    def work(self):
        self._log("bot started")
        try:
            self._bot.send_message(chat_id=100806533, text="bot started", reply_markup=telegram.ReplyKeyboardRemove())
            self._updater.start_polling()
        except telegram.error.NetworkError as ex:
            self._log("network failed: \n%s" % traceback.format_exc())
        except Exception as ex:
            self._log("bot failed: \n%s" % traceback.format_exc())
            raise

    def _handle_query(self, update, context):
        self._log("query from %d: '%s'" % (
            update.callback_query.from_user.id,
            str(update.callback_query)))
        print("Query %d '%s'" % (update.callback_query.from_user.id, update.callback_query.data))
        if update.callback_query.data == "/start":
            self.reception.handle_start(update.callback_query.from_user.id, update.callback_query.data)
        else:
            self.reception.handle_query(update)

    def _handle_telegram_error(self, update, context):
        self.logger.error("t_error handled: '%s'" % context.error.__class__.__name__)

    def _start(self, update, context):
        self._log("/start from %d: '%s'" % (
            update.message.from_user.id,
            str(update.message)))
        print("/start %d '%s'" % (update.message.from_user.id, update.message.text))
        self.reception.handle_start(update.message.from_user.id, update.message.text)

    def _echo(self, update, context):
        self._log("echo from %d: '%s'" % (
            update.message.from_user.id,
            str(update.message)))
        text = update.message.text
        print("read %d '%s'" % (update.message.from_user.id, text if len(text) <= 20 else text[:20] + "..."))
        try:
            self.reception.handle_echo(update)
        except Exception as ex:
            self._log("core: _echo exception: " + traceback.format_exc())


    def _log(self, msg):
        print(msg)
        self.logger.info(msg)

    def close(self):
        self._db_session.commit()

    def load_config(self, filename):
        with open(filename) as json_data_file:
            self._config = json.load(json_data_file)

