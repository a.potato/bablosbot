#!/usr/bin/python
# coding=utf-8

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, String, ForeignKey, create_engine, Table, Boolean
from sqlalchemy.orm import sessionmaker, relationship, scoped_session

Base = declarative_base()

class Transaction(Base):
    __tablename__ = 'transactions'

    id = Column(Integer, primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'))
    name = Column(String)

    payments = relationship("Payment", back_populates="transaction",
                            cascade="all, delete, delete-orphan")
    group = relationship("Group", back_populates="transactions")

    def __repr__(self):
        return "<Transaction(name='%s', group_id='%s')>" % (
            self.name, self.group_id
        )


class Group(Base):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    invite_code = Column(Integer, unique=True)
    gid = Column(Integer, unique=True)

    members = relationship("Member", back_populates="group")
    transactions = relationship("Transaction", order_by=Transaction.id, back_populates="group")

    def __repr__(self):
        return "<Group(id='%s', name='%s', gid='%s', invite_code='%s')>" % (
            self.id, self.name, self.gid, self.invite_code
        )

class Member(Base):
    __tablename__ = 'members'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    group_id = Column(Integer, ForeignKey('groups.id'))
    archived = Column(Boolean, default=False)

    group = relationship("Group", back_populates='members')
    assoc_user = relationship("AssocUserMember", back_populates='member', uselist=False)
    payments = relationship("Payment", back_populates="member")

    def __repr__(self):
        return "<Member(id='%s', name='%s', group_id='%s')>" % (
            self.id, self.name, self.group_id
        )


class AssocUserMember(Base):
    __tablename__ = 'assoc_user_member'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    member_id = Column(Integer, ForeignKey('members.id'))

    user = relationship("User", back_populates='assoc_members', uselist=False)
    member = relationship("Member", back_populates='assoc_user')

    def __repr__(self):
        return "<AssocUserMember(id='%s', user_id='%s', member_id='%s')>" % (
            self.id, self.user_id, self.member_id
        )

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    telegram_uid = Column(Integer)
    name = Column(String)

    assoc_members = relationship("AssocUserMember", back_populates="user")

    def __repr__(self):
        return "<User(telegram_uid='%s', name='%s')>" % (
            self.telegram_uid, self.name
        )


class Payment(Base):
    __tablename__ = 'payments'

    id = Column(Integer, primary_key=True)
    transaction_id = Column(Integer, ForeignKey('transactions.id'))
    member_id = Column(Integer, ForeignKey('members.id'))
    amount = Column(Float)
    rate = Column(Float)

    transaction = relationship("Transaction", back_populates="payments")
    member = relationship("Member", back_populates="payments")

    def __repr__(self):
        return "<Payment(transaction_id='%s', amount='%s', rate='%s', member_id='%s')>" % (
            self.transaction_id, self.amount, self.rate, self.member_id)

def create_session(url):
    engine = create_engine(url, echo=False)
    Base.metadata.create_all(engine)

    # column = Column('hidden', Boolean, default=False)
    # add_column(engine, "assoc_user_groups", column)

    session_factory = sessionmaker(bind=engine)
    Session = scoped_session(session_factory)
    session = Session()

    return session


def add_column(engine, table_name, column):
    column_name = column.compile(dialect=engine.dialect)
    column_type = column.type.compile(engine.dialect)
    sql_request = 'ALTER TABLE %s ADD COLUMN %s %s' % (table_name, column_name, column_type)
    print(sql_request)
    engine.execute(sql_request)
