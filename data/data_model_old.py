#!/usr/bin/python
# coding=utf-8

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, String, ForeignKey, create_engine, Table, Boolean
from sqlalchemy.orm import sessionmaker, relationship, scoped_session

Base = declarative_base()

user_groups = Table('user_groups', Base.metadata,
                    Column('user_id', ForeignKey('users.id'), primary_key=True),
                    Column('group_id', ForeignKey('groups.id'), primary_key=True))


class Transaction(Base):
    __tablename__ = 'transactions'

    id = Column(Integer, primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'))
    name = Column(String)

    payments = relationship("Payment", back_populates="transaction",
                            cascade="all, delete, delete-orphan")
    group = relationship("Group", back_populates="transactions")

    def __repr__(self):
        return "<Transaction(name='%s', group_id='%s')>" % (
            self.name, self.group_id
        )


class Group(Base):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    invite_code = Column(Integer, unique=True)
    gid = Column(Integer, unique=True)

    users = relationship("AssociationUserGroups", back_populates="group")
    transactions = relationship("Transaction", order_by=Transaction.id, back_populates="group")

    def __repr__(self):
        return "<Group(id='%s', name='%s', gid='%s', invite_code='%s')>" % (
            self.id, self.name, self.gid, self.invite_code
        )


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    telegram_uid = Column(Integer)
    name = Column(String)

    groups = relationship("AssociationUserGroups", back_populates="user")
    payments = relationship("Payment", back_populates="user")

    def __repr__(self):
        return "<User(telegram_uid='%s', name='%s')>" % (
            self.telegram_uid, self.name
        )


class Payment(Base):
    __tablename__ = 'payments'

    id = Column(Integer, primary_key=True)
    transaction_id = Column(Integer, ForeignKey('transactions.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    amount = Column(Float)
    rate = Column(Float)

    transaction = relationship("Transaction", back_populates="payments")
    user = relationship("User", back_populates="payments")

    def __repr__(self):
        return "<Payment(transaction_id='%s', amount='%s', rate='%s')>" % (
            self.transaction_id, self.amount, self.rate)


class AssociationUserGroups(Base):
    __tablename__ = 'assoc_user_groups'

    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'), primary_key=True)
    username = Column(String)
    hidden = Column(Boolean, default=False)

    user = relationship('User', back_populates="groups")
    group = relationship('Group', back_populates="users")

    def __repr__(self):
        return "<AssociationUserGroups(user_id='%s', group_id='%s', username='%s', hidden='%s')>" % (
            self.user_id, self.group_id, self.username, str(self.hidden)
        )

def create_session(url):
    engine = create_engine(url, echo=False)
    Base.metadata.create_all(engine)

    # column = Column('hidden', Boolean, default=False)
    # add_column(engine, "assoc_user_groups", column)

    session_factory = sessionmaker(bind=engine)
    Session = scoped_session(session_factory)
    session = Session()

    return session


def add_column(engine, table_name, column):
    column_name = column.compile(dialect=engine.dialect)
    column_type = column.type.compile(engine.dialect)
    sql_request = 'ALTER TABLE %s ADD COLUMN %s %s' % (table_name, column_name, column_type)
    print(sql_request)
    engine.execute(sql_request)
