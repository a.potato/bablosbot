from data import data_model_old as dm
from data import data_model as dm2


class TestCreate_session():
    def __init__(self):
        self.sess = None
        self.sess2 = None

    def test_create_session(self):
        session = dm.create_session('sqlite:///')
        userA = dm.User(name="A", telegram_uid=1)
        session.add(userA)
        groupB = dm.Group(name="B", invite_code=2, gid=3)
        session.add(groupB)

        assoc = dm.AssociationUserGroups(username=userA.name)
        assoc.user = userA
        groupB.users.append(assoc)

        print("hh")
        pass

    def test_make_new_db(self):
        sess = dm.create_session('sqlite:///../database_old.db')
        count = sess.query(dm.User.telegram_uid).count()
        print(count)

        sess2 = dm2.create_session('sqlite:///../database.sqlite')
        # sess2 = dm2.create_session('sqlite:///')

        # Copy users
        print("copy users", end='')
        old_users = sess.query(dm.User)
        for old_u in old_users:
            new_u = dm2.User(id=old_u.id,
                             telegram_uid=old_u.telegram_uid,
                             name=old_u.name)
            sess2.add(new_u)
            print('.', end='')
        print(' done!')

        # create groups
        print("copy groups...", end='')
        old_groups = sess.query(dm.Group)
        for old_g in old_groups:
            new_g = dm2.Group(id=old_g.id,
                              name=old_g.name,
                              gid=old_g.gid,
                              invite_code=old_g.invite_code)
            sess2.add(new_g)
            print('.', end='')
        print(' done!')

        # create member participations
        print("copy member-group association...", end='')
        for old_u in old_users:
            for assoc_group in old_u.groups:
                old_g = assoc_group.group
                new_g = sess2.query(dm2.Group).filter_by(id=old_g.id).one()
                new_u = sess2.query(dm2.User).filter_by(id=old_u.id).one()

                member = dm2.Member(name=assoc_group.username,
                                    group=new_g,
                                    archived=assoc_group.hidden)
                assoc_member = dm2.AssocUserMember(member=member,
                                                   user=new_u)
                sess2.add(member)
            print('.', end='')
        print(' done!')

        print("copy transactions...", end='')
        # create payments and transactions
        for old_g in old_groups:
            for old_trans in old_g.transactions:
                group = sess2.query(dm2.Group).filter_by(
                    id=old_trans.group.id).one()
                trans = dm2.Transaction(id=old_trans.id,
                                        name=old_trans.name,
                                        group=group)

                for old_payment in old_trans.payments:
                    # replace payment.user with payment.member
                    member = sess2.query(dm2.Member)
                    member = member.join(dm2.AssocUserMember)
                    member = member.join(dm2.User)
                    member = member.join(dm2.Group)
                    member = member.filter(dm2.User.id == old_payment.user.id)
                    member = member.filter(dm2.Group.id == group.id)
                    member = member.one()

                    assert \
                        member.assoc_user.user.telegram_uid == \
                        old_payment.user.telegram_uid, \
                        "telegram_uid is broken"

                    # user = sess2.query(dm2.User).filter_by(
                    #     id=old_payment.user.id).one()
                    payment = dm2.Payment(id=old_payment.id,
                                          amount=old_payment.amount,
                                          rate=old_payment.rate,
                                          member=member,
                                          transaction=trans)
            print('.', end='')
        print(' done!')

        print("done copy.")
        sess2.commit()
        self.sess2 = sess2
        self.sess = sess

    def read_old_db(self):
        print('\n OLD DB')
        users = self.sess.query(dm.User).all()
        # print("Users in olddb:")
        for user in users:
            # print(user)
            pass

        groups = self.sess.query(dm.Group).all()
        print("Groups in db:")
        for g in groups:
            print(g)
            names = ', '.join([assoc.username for assoc in g.users])
            print(names)
            for assoc_u_g in g.users:
                # print(assoc_u_g)
                pass



    def read_new_db(self):
        print('\n NEW DB')
        users = self.sess2.query(dm2.User).all()
        # print("Users in db:")
        for user in users:
            # print(user)
            pass

        groups = self.sess2.query(dm2.Group).all()
        print("Groups in db:")
        for g in groups:
            print(g)
            names = ', '.join([member.name for member in g.members])
            print(names)

            for member in g.members:
                # print(member)
                # print(member.assoc_user)
                pass

            for trans in g.transactions:
                print(trans)
                for payment in trans.payments:
                    print(payment.member.name + " " + str(payment))

        telegram_uid = self.sess2.query(dm2.User.telegram_uid) \
            .join(dm2.AssocUserMember) \
            .filter(dm2.AssocUserMember.member_id == 1) \
            .one()

        print("t_uid=%s" % telegram_uid)

    def compare_db(self):
        old_users = self.sess.query(dm.User).all()
        new_users = self.sess2.query(dm2.User).all()
        print("Users in olddb:")
        for user in old_users:
            print(user)

        old_groups = self.sess.query(dm.Group).all()
        print("Groups in db:")
        for g in old_groups:
            print(g)
            names = ', '.join([assoc.username for assoc in g.users])
            print(names)
            for assoc_u_g in g.users:
                # print(assoc_u_g)
                pass

        print('\n NEW DB')
        new_users = self.sess2.query(dm2.User).all()
        print("Users in db:")
        for user in new_users:
            print(user)
            pass

        new_groups = self.sess2.query(dm2.Group).all()
        print("Groups in db:")
        for g in new_groups:
            print(g)
            names = ', '.join([member.name for member in g.members])
            print(names)
            for member in g.members:
                # print(member)
                # print(member.assoc_user)
                pass


if __name__ == "__main__":
    t = TestCreate_session()
    # t.test_create_session()
    t.test_make_new_db()
    t.read_old_db()
    t.read_new_db()
