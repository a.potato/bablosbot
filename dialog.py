#!/usr/bin/python
# coding=utf-8
import logging
import traceback
import telegram as t

class Dialog(object):
    def __init__(self, bot, chat_id, user_id, accountant):
        self._bot = bot
        self._chat_id = chat_id
        self._user_id = user_id
        self._accountant = accountant
        self.responders = {}
        self.query_handlers = {}
        self.markups = {}
        self.current_dialog = None
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)
        self.last_messages = {}

        self.buttons = {'cancel': t.InlineKeyboardButton(u'\u21ba', callback_data="/start"),
                        'done': t.InlineKeyboardButton(u'\u2713', callback_data="/done"),
                        }
        self.markup_inline_empty = t.InlineKeyboardMarkup([[]])
        pass

    def is_finished(self):
        raise NotImplementedError()

    def is_started(self):
        raise NotImplementedError()

    def send_message(self, text, **kwargs):
        # TODO replace with MessageQueue
        if 'reply_markup' not in kwargs.keys():
            kwargs['reply_markup'] = \
                self.markups[self.state] if self.state in self.markups.keys() else \
                    t.ReplyKeyboardRemove()

        if 'text' not in kwargs.keys():
            kwargs['text'] = text
        if 'chat_id' not in kwargs.keys():
            kwargs['chat_id'] = self._chat_id
        elif kwargs['chat_id'] != self._chat_id:
            kwargs['reply_markup'] = None

        msg = None
        try:
            if 'second_reply_markup' in kwargs.keys():
                second_markup = kwargs['second_reply_markup']
                second_msg = self._bot.send_message(chat_id=self._chat_id,
                                                    text="выбери кнопку ↓",
                                                    reply_markup=second_markup)
                self.logger.info("SENT_: '%s'" % str(second_msg))
                print("sent_%d" % self._chat_id)

            msg = self._bot.send_message(**kwargs)
            self.logger.info("SENT : '%s'" % str(msg))
            print("sent %d '%s'" % (self._chat_id, text if len(text) <= 20 else text[:20] + "..."))

        except t.error.NetworkError as ex:
            self.logger.error("send_message had NetworkError: %s" % traceback.format_exc())
            print("send_message had NetworkError: %s" % str(ex))
        except t.error.Unauthorized as ex:
            self.logger.error("send_message had Unauthorized: %s" % traceback.format_exc())
            print("send_message had Unauthorized: %s" % str(ex))

        return msg

    def send_message_savemarkup(self, text, **kwargs):
        msg = self.send_message(text, **kwargs)
        self.last_messages[self.state] = msg
        return msg

    def process(self, text):
        self.logger.debug("Process with %s (chat_id=%d): '%s'" % (
                self.__class__.__name__,
                self._user_id,
                text))
        if self.current_dialog is not None:
            try:
                self.current_dialog.process(text)
            except Exception as ex:
                text = "EXCEPT in %s.current_dialog[%s].process(): %s" % (
                    self.__class__.__name__,
                    self.state,
                    traceback.format_exc())
                self.logger.error(text)
                print(text)
                self.send_message("При обработке команды возникла ошибка, попробуй начать сначала: /start")
                self.notify("exception")

        if self.state in self.responders.keys():
            try:
                self.responders[self.state](text)
            except Exception as ex:
                text = "EXCEPT in %s.responders[%s](): %s" % (
                    self.__class__.__name__,
                    self.state,
                    traceback.format_exc())
                self.logger.error(text)
                print(text)
                self.send_message("При обработке команды возникла ошибка, попробуй начать сначала: /start")
                self.notify("exception")

        if self.current_dialog is not None and self.current_dialog.is_finished():
            self.current_dialog = None

    def handle_query(self, callback_query):
        self.logger.debug("Query with %s (chat_id=%d): '%s'" % (
                self.__class__.__name__,
                self._user_id,
                callback_query.data))
        if self.current_dialog is not None:
            try:
                self.current_dialog.handle_query(callback_query)
            except Exception as ex:
                self.logger.error(traceback.format_exc())
                self.send_message("При обработке кнопки возникла ошибка, попробуй начать сначала: /start")
                self.notify("exception")

        if self.state in self.query_handlers.keys():
            try:
                self.query_handlers[self.state](callback_query)
            except Exception as ex:
                text = "EXCEPT in %s.query_handlers[%s](): %s" % (
                    self.__class__.__name__,
                    self.state,
                    traceback.format_exc())
                self.logger.error(text)
                print(text)
                self.send_message("При обработке кнопки возникла ошибка, попробуй начать сначала: /start")

        if self.current_dialog is not None and self.current_dialog.is_finished():
            self.current_dialog = None

    def rounded_string(self, from_decimal):
        s = ('%+.2f' % from_decimal).rstrip('0').rstrip('.')
        if s == '+0' or s == '-0':
            s = '0'
        return s

    def notify(self, text):
        try:
            self._bot.send_message(chat_id=100806533, text="notify: " + text)
        except Exception:
            pass