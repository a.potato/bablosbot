#!/usr/bin/python
# coding=utf-8

from dialog import Dialog
import telegram as t
from transitions import Machine, MachineError, State

from dialog_newgroup import DialogNewgroup
from dialog_group import DialogGroup
import re


class DialogFront(Dialog):
    def __init__(self, bot, chat_id, user_id, accountant, parameter=None):
        super().__init__(bot, chat_id, user_id, accountant)

        self.selected_group = None
        self.user_groups = []
        self.message_with_markup = None

        states = ['init',
                  State(name='group_list', on_enter='say_group_list', on_exit='bye_group_list'),
                  State(name='group', on_enter='say_group'),
                  State(name='welcome', on_enter='say_welcome'),
                  State(name='create_group', on_enter='say_create_group', on_exit='bye_create_group'),
                  State(name='deeplinked', on_enter='say_deeplinked'),
                  State(name='delete_group', on_enter='say_delete_group'),
                  State(name='archive', on_enter='say_archive'),
                  ]
        transitions = [

            {'trigger': 'группы', 'source': 'welcome', 'dest': 'group_list'},
            {'trigger': 'to_group', 'source': 'group_list', 'dest': 'group'},
            {'trigger': 'back_to_list', 'source': 'group', 'dest': 'group_list'},
            {'trigger': 'create_group', 'source': 'welcome', 'dest': 'create_group'},
            {'trigger': 'single_group', 'source': 'welcome', 'dest': 'group'},
            {'trigger': 'created_group', 'source': 'create_group', 'dest': 'group'},
            {'trigger': 'cancel', 'source': 'create_group', 'dest': 'group_list'},
            {'trigger': 'создать группу', 'source': 'group_list', 'dest': 'create_group'},
            {'trigger': 'deeplink_accepted', 'source': 'deeplinked', 'dest': 'group'},
            {'trigger': 'deeplink_refused', 'source': 'deeplinked', 'dest': 'welcome'},
            {'trigger': 'удалить', 'source': 'group_list', 'dest': 'delete_group'},
            {'trigger': 'deleted', 'source': 'delete_group', 'dest': 'group_list'},
            {'trigger': 'архив', 'source': 'group_list', 'dest': 'archive'},
            {'trigger': 'dearchived', 'source': 'archive', 'dest': 'group_list'},
            {'trigger': 'to_group', 'source': 'archive', 'dest': 'group'},
        ]
        self.machine = Machine(model=self, states=states, transitions=transitions, initial='init',
                               ignore_invalid_triggers=True)
        self.responders = {'welcome': self.respond_welcome,
                           'group_list': self.respond_group_list,
                           'group': self.respond_group,
                           'create_group': self.respond_create_group,
                           'deeplinked': self.respond_deeplinked,
                           'delete_group': self.respond_delete_group,
                           'archive': self.respond_archive,
                           }

        self.query_handlers['group_list'] = self.query_group_list

        group_keyboard = [['добавить чек'],
                          ['последние транзакции'],
                          ['посмотреть балансы'],
                          ['к списку групп'],
                          ['пригласить']]
        self.group_markup = t.ReplyKeyboardMarkup(group_keyboard, resize_keyboard=True)

        self.group_list_markup = t.ReplyKeyboardMarkup([['создать группу'],
                                                        ['удалить', 'архив']], resize_keyboard=True)

        self.markups = {'group': self.group_markup,
                        'group_list': self.group_list_markup, }

        # self.send_message = lambda text, reply_markup, state=self.markups[self.state]: \
        # self._bot.send_message(text=text, reply_markup=reply_markup)

        if parameter:
            self.parameter = parameter
            self.to_deeplinked()
        else:
            self.to_welcome()

    def process(self, text):
        if text == '/start':
            self.to_welcome()
            return
        else:
            super().process(text)

    def say_welcome(self):
        self.current_dialog = None
        msg = "Привет. Я бот - счётчик бабла, в меня удобно записывать общие покупки. " \
              "Добавляетесь все в группу, создаешь чек, указываешь, кто и за кого платил.\n" \
              "В любой непонятной ситуации напиши /start, это по крайней мере вернёт тебя сюда.\n\n" \
              "🌈 Долгожданное обновление! Теперь можно добавить оффлайн-пользователя, не заставляя его " \
              "устанавливать телеграм или подключаться к боту!"
        gids = self._accountant.get_group_ids_by_uid_visible(self._user_id)
        if gids is None:
            msg += "\nСейчас у тебя нет ни одной группы. Создай или присоединись."
            self.send_message(text=msg)
            self.trigger('create_group')
        elif len(gids) == 1:
            msg += "\nУ тебя одна группа, '%s', захожу в нее" % self._accountant.get_group_name(gids[0])
            self.selected_group = gids[0]
            self.send_message(text=msg)
            self.trigger('single_group')
        else:
            self.send_message(text=msg)
            self.trigger('группы')

    def respond_welcome(self, text):
        if text.lower() in self.machine.get_triggers(self.state):
            self.trigger(text.lower())
        else:
            self.send_message("Не понимаю чего ты хочешь. Кнопки должны быть, жми их")

    def say_create_group(self):
        assert self.current_dialog is None, "предыдущий диалог не завершен"
        self.current_dialog = DialogNewgroup(self._bot, self._chat_id, self._user_id, self._accountant)

    def respond_create_group(self, text):
        if self.current_dialog.is_finished():
            selected_gid = self.current_dialog.get_result()
            if selected_gid:
                self.selected_group = selected_gid
                self.trigger('created_group')
            else:
                self.trigger('cancel')

    def bye_create_group(self):
        self.current_dialog = None

    def say_group_list(self):
        gids = self._accountant.get_group_ids_by_uid_visible(self._user_id)
        if gids is None:
            self.send_message("Групп-то нет!")
        else:
            gnames = [self._accountant.get_group_name(gid) for gid in gids]
            gnums = list(range(1, len(gnames) + 1))
            msg = '\n'.join(["%d. %s" % (num, name) for (num, name) in zip(gnums, gnames)])

            if len(gnames) <= 5:
                # buttons = [[]]
                buttons = [[t.InlineKeyboardButton(
                    gnames[i], callback_data=str(gnums[i]))]
                    for i in range(len(gnames))]
                #buttons.append([t.InlineKeyboardButton(
                #    "+", callback_data="создать группу")])
                self.send_message("Выбери группу или создай новую:")
                answer = self.send_message("Твои группы:", reply_markup=t.InlineKeyboardMarkup(buttons))
                self.message_with_markup = answer.message_id
            else:
                self.send_message("Введи номер группы:\n%s" % msg)

            self.user_groups = gids

    def query_group_list(self, callback_query):
        text = callback_query.data
        if text.lower() in self.machine.get_triggers(self.state):
            # callback_query.edit_message_reply_markup(
            #     reply_markup=t.InlineKeyboardMarkup([[]]))
            self.trigger(text.lower())
        else:
            try:
                selected_num = int(text) - 1  # from 0
                if selected_num >= 0 and selected_num < len(self.user_groups):
                    self.selected_group = self.user_groups[selected_num]
                    # callback_query.edit_message_reply_markup(
                    #     reply_markup=t.InlineKeyboardMarkup([[]]))
                    self.trigger('to_group')
                else:
                    self.send_message("^ Из этих номеров")
            except ValueError:
                self.send_message('Введи номер выбранной группы')

    def respond_group_list(self, text):
        if text.lower() in self.machine.get_triggers(self.state):
            self.trigger(text.lower())
        else:
            try:
                selected_num = int(text) - 1  # from 0
                if selected_num >= 0 and selected_num < len(self.user_groups):
                    self.selected_group = self.user_groups[selected_num]
                    # if self.message_with_markup is not None:
                    #     self._bot.edit_message_reply_markup(chat_id=self._chat_id,
                    #                                         message_id=self.message_with_markup,
                    #                                         reply_markup=t.InlineKeyboardMarkup([[]]))
                    self.trigger('to_group')
                else:
                    self.send_message("^ Из этих номеров")
            except ValueError:
                self.send_message('Введи номер выбранной группы')

    def bye_group_list(self):
        if self.message_with_markup is not None:
            self._bot.edit_message_reply_markup(chat_id=self._chat_id, message_id=self.message_with_markup,
                                                reply_markup=t.InlineKeyboardMarkup([[]]))
            self.message_with_markup = None

    def say_group(self):
        self._accountant.set_group_archived(self._user_id, self.selected_group, archived=False)
        self.current_dialog = DialogGroup(self._bot, self._chat_id, self._user_id, self._accountant,
                                          self.selected_group)

    def respond_group(self, text):
        if self.current_dialog.is_finished():
            self.current_dialog = None
            self.trigger('back_to_list')

    def say_deeplinked(self):
        text = str(self.parameter)
        is_accepted = False
        self.logger.debug("processing deeplink parameter: '%s'" % text)
        if re.fullmatch(r'\d+', text):
            self.logger.debug("deeplink parameter matches '\d+'")
            try:
                invite_code = int(text)
                self.logger.debug("deeplink parameter parsed: '%d'" % invite_code)
                gid = self._accountant.find_gid_by_invite(invite_code)
                if gid is not None:
                    self.selected_group = gid
                    g_name = self._accountant.get_group_name(gid)
                    uids = self._accountant.get_uids_from_group(gid)
                    if self._user_id not in uids:
                        names = self._accountant.get_names_from_group(gid)
                        count = len(names)
                        message = "Нашёл по ссылке группу %s\nздесь " % g_name
                        message += "никого нет" if count == 0 else \
                            "один человек, %s" % names[0] if count == 1 else \
                            "двое, %s и %s" % tuple(names) if count == 2 else \
                            "трое: %s, %s и %s" % tuple(names) if count == 3 else \
                            "%d человек%s:\n%s" % (count, "а" if count == 4 else "",
                                                   ', '.join(names))
                                  #"%d человек:\n" % (g_name, len(uids))
                        self.send_message(message)
                        self.send_message("Как записать твоё имя?")
                    else:
                        #self.send_message(text="Ты уже в этой группе. Захожу в нее...")
                        self.trigger('deeplink_accepted')
                else:
                    self.send_message(text="Я не нашёл группу по ссылке. Проверь")
                    self.trigger('deeplink_refused')

            except ValueError:
                self.send_message(text="Какая-то у тебя неправильная ссылка")
                self.trigger('deeplink_refused')
        else:
            self.send_message(text="Какая-то у тебя неправильная ссылка")
            self.trigger('deeplink_refused')

    def respond_deeplinked(self, text):
        username = text
        group_name = self._accountant.get_group_name(self.selected_group)
        text = "Добавил тебя в '%s', захожу" % group_name

        # Уведомление других участников о прибавлении
        uids = self._accountant.get_uids_from_group(self.selected_group)
        report = "В группе '%s' пополнение: '%s'" % (group_name, username)

        self._accountant.add_user_to_group(uid=self._user_id, gid=self.selected_group, name=username)
        self.send_message(text)
        for telegram_uid in uids:
            self.send_message(chat_id=telegram_uid, text=report)
        self.trigger('deeplink_accepted')

    def say_delete_group(self):
        self.send_message("Какой номер удалить?")

    def respond_delete_group(self, text):
        try:
            selected_num = int(text) - 1  # from 0
            if selected_num >= 0 and selected_num < len(self.user_groups):
                gid = self.user_groups[selected_num]
                self._accountant.set_group_archived(self._user_id, gid)
                gname = self._accountant.get_group_name(gid)
                self.send_message("Убрал группу '%s' в архив" % gname)
                self.trigger('deleted')
            else:
                self.send_message('Неправильный номер')
                self.trigger('deleted')
        except ValueError:
            self.send_message('Не смог прочитать номер')
            self.trigger('deleted')

    def say_archive(self):
        gids = self._accountant.get_group_ids_by_uid_hidden(self._user_id)

        if gids is None:
            self.send_message("В архиве пусто!")
            self.trigger('dearchived')
        else:
            gnames = [self._accountant.get_group_name(gid) for gid in gids]
            gnums = list(range(1, len(gnames) + 1))
            msg = '\n'.join(["%d. %s" % (num, name) for (num, name) in zip(gnums, gnames)])
            self.send_message("Выбери номер группы в архиве:\n%s" % msg)

            self.user_groups = gids

    def respond_archive(self, text):
        try:
            selected_num = int(text) - 1  # from 0
            if selected_num >= 0 and selected_num < len(self.user_groups):
                self.selected_group = self.user_groups[selected_num]
                self.trigger('to_group')
            else:
                self.send_message("Неправильный номер")
                self.trigger('dearchived')
        except ValueError:
            self.send_message('Не смог прочитать номер')
            self.trigger('dearchived')

