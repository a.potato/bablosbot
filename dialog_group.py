#!/usr/bin/python
# coding=utf-8

from dialog import Dialog
import telegram
from transitions import Machine, MachineError, State
from dialog_pay import DialogPay
from data import data_model as dm

class DialogGroup(Dialog):
    def __init__(self, bot, chat_id, user_id, accountant, gid):
        super().__init__(bot, chat_id, user_id, accountant)

        self.gid = gid
        self.last_transactions = []

        states = [State(name='init'),
                  State(name='group', on_enter='say_group', on_exit='bye_group'),
                  State(name='add_payment', on_enter='say_add_payment'),
                  State(name='finished'),
                  State(name='management', on_enter='say_management'),
                  State(name='remove_trans', on_enter='say_remove_trans'),
                  State(name='list_transactions', on_enter='say_list_transactions'),
                  State(name='edit_trans', on_enter='say_edit_trans'),
                  State(name='add_shadow', on_enter='say_add_shadow'),
                  ]
        transitions = [
            {'trigger': 'add_payment', 'source': 'group', 'dest': 'add_payment'},
            {'trigger': 'added_payment', 'source': 'add_payment', 'dest': 'group'},
            {'trigger': 'manage', 'source': 'group', 'dest': 'management'},
            {'trigger': 'managed', 'source': 'management', 'dest': 'group'},
            {'trigger': 'to_list', 'source': 'management', 'dest': 'finished'},
            {'trigger': 'remove_trans', 'source': 'list_transactions', 'dest': 'remove_trans'},
            {'trigger': 'removed_trans', 'source': 'remove_trans', 'dest': 'group'},
            {'trigger': 'payment_list', 'source': 'management', 'dest': 'list_transactions'},
            #{'trigger': 'edit_trans', 'source': 'mana'}
            {'trigger': 'from_management', 'source': 'management', 'dest': 'group'},
            {'trigger': 'add_shadow', 'source': 'group', 'dest': 'add_shadow'},
            {'trigger': 'added_shadow', 'source': 'add_shadow', 'dest': 'group'},
            ]

        self.machine = Machine(model=self, states=states, transitions=transitions, initial='init',
                               ignore_invalid_triggers=True)

        self.responders = {'group': self.respond_group,
                           'add_payment': self.respond_add_payment,
                           'management': self.respond_management,
                           'list_transactions': self.respond_list_transactions,
                           'remove_trans': self.respond_remove_trans,
                           'add_shadow': self.respond_add_shadow,
                           }
        self.query_handlers = {'add_payment': self.query_add_payment,
                               #'remove_trans': self.query_remove_trans,
                               'group': self.query_group,
                               }

        group_keyboard = [['добавить чек'],
                          ['последние чеки'],
                          ['баланс'],
                          ['управление'],
                          ]
        group_markup = telegram.ReplyKeyboardMarkup(group_keyboard, resize_keyboard=True)

        management_keyboard = [['добавить участника'],
                               ['изменить чеки'],
                               ['к списку групп'],
                               ['назад']
                               ]
        management_markup = telegram.ReplyKeyboardMarkup(management_keyboard,
                                                         resize_keyboard=True,
                                                         one_time_keyboard=True)
        list_transactions_markup = telegram.ReplyKeyboardMarkup([['удалить'],
                                                         ['редактировать'],
                                                         ['смотреть все'],
                                                         ['назад']],
                                                        resize_keyboard=False)

        self.markups = {'group': group_markup,
                        'add_payment': group_markup,
                        'management': management_markup,
                        'list_transactions': list_transactions_markup,
                        'add_shadow': telegram.ReplyKeyboardMarkup([['назад']],
                                                                   resize_keyboard=True,
                                                                   one_time_keyboard=True),
                        }

        self.to_group()

    def say_group(self):
        gid = self.gid
        gname = self._accountant.get_group_name(gid)
        message = "Группа '%s'\n" % gname
        member_self = self._accountant._member(self._user_id, gid)
        message += "Баланс: %s" % self.rounded_string(self._accountant.get_member_credit(member_self.id))
        self.send_message(message)

    def respond_group(self, text):
        if text == 'добавить чек':
            self.trigger('add_payment')
        elif text == "последние чеки":
            last_transactions = self._accountant.show_last_transactions(self.gid, 10)
            self.send_message(last_transactions)
        elif text == 'баланс':
            names_balances = self._accountant.get_names_balances(self.gid)
            report = "Балансы:"
            for (name, balance) in names_balances:
                report += "\n%s: %s" % (name, self.rounded_string(balance))
            self.send_message(report)
        elif text == 'управление':
            self.trigger('manage')

    def query_group(self, callback_query):
        if callback_query.data == "/add_offline":
            self.trigger('add_shadow')

    def bye_group(self):
        # remove inline button for invite_offline
        # search for saved message with msg_id as key
        for key, msg in self.last_messages.items():
            if isinstance(key, int):
                self._bot.edit_message_reply_markup(chat_id=self._chat_id,
                                                    message_id=msg.message_id,
                                                    reply_markup=telegram.InlineKeyboardMarkup([[]]))
                del self.last_messages[key]
                break

    def say_add_payment(self):
        self.current_dialog = DialogPay(self._bot, self._chat_id, self._user_id, self._accountant,
                                        self.markups[self.state], self.gid)
        self.current_dialog.process("")
        self.respond_add_payment("")

    def respond_add_payment(self, text):
        if self.current_dialog.is_finished():
            self.current_dialog = None
            self.trigger('added_payment')

    def query_add_payment(self, callback_query):
        if self.current_dialog.is_finished():
            self.current_dialog = None
            self.trigger('added_payment')

    def say_management(self):
        self.send_message("Управление группой")

    def respond_management(self, text):
        if text == 'добавить участника':
            invite = self._accountant.get_invite_by_gid(self.gid)
            link = """https://t.me/%s?start=%s""" % (self._bot.username, str(invite))
            self.send_message("Ссылка для добавления в группу. Отправь ее тому, кого приглашаешь.")

            last_row = [[telegram.InlineKeyboardButton("добавить оффлайн-участника", callback_data="/add_offline")]]
            keyboard_markup = telegram.InlineKeyboardMarkup(last_row)
            msg = self.send_message(str(link), reply_markup=keyboard_markup)
            msg_id = msg.message_id
            self.last_messages[msg_id] = msg

            self.trigger('managed')

        elif text == 'изменить чеки':
            self.trigger('payment_list')

        elif text == 'к списку групп':
            self.trigger('to_list')

        elif text == 'назад':
            self.trigger('from_management')

    def say_list_transactions(self):
        self.last_transactions = self._accountant.transactions_from_group(self.gid)
        text = "Чеки:\n" if len(self.last_transactions) > 0 else "Чеков в группе пока нет"
        for i in range(len(self.last_transactions)):
            trans = self.last_transactions[i]
            text += str(i+1) + ". " + trans.name
            text += ": "

            texts_list = []
            for payment in trans.payments:
                if payment.amount == 0:
                    continue

                username = self._accountant.get_member_name(member_id=payment.member_id)
                texts_list.append("%s %g" % (username, payment.amount))
            text += ", ".join(texts_list) + "\n"

        self.send_message(text)

    def respond_list_transactions(self, text):
        if text == 'удалить':
            self.trigger('remove_trans')
        elif text == 'редактировать':
            self.send_message('функция не готова')
            self.trigger('to_group')
        elif text == 'смотреть все':
            self.trigger('to_list_transactions')
        elif text == 'назад':
            self.trigger('to_group')
        else:
            self.send_message("Нажми одну из кнопок")

    def say_remove_trans(self):
        self.send_message("Какой номер удалить?")

    def respond_remove_trans(self, text):
        try:
            id = int(text) - 1
        except ValueError:
            self.send_message("Ошибка, пришло не число")
            self.trigger('removed_trans')
            return

        if id >= len(self.last_transactions) or id < 0:
            self.send_message("Введи номер чека из перечисленных")
            return

        deleted = self.last_transactions[id]
        #trans_string = self._accountant.transaction_string(deleted.id)

        trans_members_ids = list(set([payment.member_id for payment in deleted.payments]))
        # users_list = self._accountant.user_from_userid(user_id_list)
        # До удаления чека, посчитать его для уведомлений
        check_credits = {}
        for member_id in trans_members_ids:
            check_credits[member_id] = self._accountant.member_trans_credit(deleted, member_id)

        # Удалить чек
        self._accountant.remove_transaction(deleted.id)
        self.send_message("Получилось, удалил '%s'" % deleted.name)

        # Уведомления участникам чека
        # Имя группы
        group_name = self._accountant.get_group_name(self.gid)
        # имя удалившего чек
        remover_name = self._accountant.get_member_name(telegram_uid=self._user_id,
                                                        gid=self.gid)
        # member_id удалившего чек
        remover_member = self._accountant._member(self._user_id, self.gid)
        # Отправить уведомления участникам
        for member_id in trans_members_ids:
            # Получить telegram_uid членов группы
            telegram_uid = self._accountant.get_telegram_uid_from_member(member_id)
            if not telegram_uid:
                # Не отправлять членам без телеграмма
                continue
            if remover_member.id == member_id:
                # Не отправлять автору запроса
                continue

            user_credit = self._accountant.get_member_credit(member_id)
            user_trans_credit = check_credits[member_id]
            message = "Группа '%s'. %s удалил чек '%s'.\n" % (
                group_name,
                remover_name,
                deleted.name
            ) + "Твой баланс: %s (%s)" % (
                    self.rounded_string(user_credit),
                    self.rounded_string(-user_trans_credit)) # Изменение после удаления - отрицательное число
            self.send_message(message, chat_id=telegram_uid)

        self.trigger('removed_trans')

    def say_add_shadow(self):
        self.send_message("Введи его имя")

    def respond_add_shadow(self, text):
        if text != 'назад':
            username = text
            group_name = self._accountant.get_group_name(self.gid)

            # Уведомление других участников о прибавлении
            uids = self._accountant.get_uids_from_group(self.gid)
            report = "В группе '%s' добавили оффлайн-пользователя: '%s'" % (group_name, username)

            self._accountant.add_user_to_group(uid=None, gid=self.gid, name=username)
            self.send_message("Добавил оффлайн-пользователя '%s'" % username)
            for telegram_uid in uids:
                self.send_message(chat_id=telegram_uid, text=report)

        self.trigger('added_shadow')

    def is_finished(self):
        return self.state == 'finished'

    def is_started(self):
        return self.state != 'init' and not self.is_finished()
