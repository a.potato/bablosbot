#!/usr/bin/python
# coding=utf-8

from dialog import Dialog
import telegram as t
from transitions import Machine, MachineError, State
from dialog_pay import DialogPay
import re

class DialogNewgroup(Dialog):
    def __init__(self, bot, chat_id, user_id, accountant):
        super().__init__(bot, chat_id, user_id, accountant)

        self.newgroup_name = None
        self.invite_group_id = None
        self.gid = None

        states = ['init',
                  State(name='welcome', on_enter='say_welcome'),
                  State(name='requested_name'),
                  State(name='finished')
                  ]
        transitions = [
            {'trigger': 'request_name', 'source': 'welcome', 'dest': 'requested_name'},
            {'trigger': 'invite_approved', 'source': 'welcome', 'dest': 'requested_name'},
            {'trigger': 'cancel', 'source': 'welcome', 'dest': 'finished'},
            ]

        self.machine = Machine(model=self, states=states, transitions=transitions, initial='init',
                               ignore_invalid_triggers=True)

        self.responders = {'welcome': self.respond_welcome,
                           'requested_name': self.respond_requested_name,
                           }

        self.markups = {'welcome': t.ReplyKeyboardMarkup([['назад']], resize_keyboard=True),
                        }

        self.to_welcome()

    def send_message(self, text, **kwargs):
        reply_markup = kwargs['reply_markup'] if 'reply_markup' in kwargs.keys() else \
            self.markups[self.state] if self.state in self.markups.keys() else t.ReplyKeyboardRemove()
        msg = self._bot.send_message(chat_id=self._chat_id, text=text, reply_markup=reply_markup)
        self.last_markup = reply_markup
        return msg

    def say_welcome(self):
        self.send_message("Создаём группу. Как будет называться?")

    def respond_welcome(self, text):
        if text == 'назад':
            self.trigger('cancel')
        else:
            # Received group name
            self.newgroup_name = text.replace('\n', '')
            self.send_message(text="'%s'. Как записать твое имя?" % self.newgroup_name)
            self.trigger('request_name')

    def respond_requested_name(self, text):
        username = text.replace('\n', '')
        gid = self._accountant.create_group(self.newgroup_name)
        text = "Группа '%s' создана, захожу" % self.newgroup_name
        assert gid is not None, "ошибка в логике обрабоки - gid is None"

        self._accountant.add_user_to_group(uid=self._user_id, gid=gid, name=username)
        self.gid = gid
        self.send_message(text)

        self.to_finished()

    def get_result(self):
        return self.gid

    def is_finished(self):
        return self.state == 'finished'

    def is_started(self):
        return self.state != 'init' and not self.is_finished()
