#!/usr/bin/python
# coding=utf-8

import accountant
from dialog import Dialog
from transitions import Machine, State
import re
import telegram as t


class DialogPay(Dialog):
    def __init__(self, bot, chat_id, user_id, accountant, menu_markup, gid):
        super().__init__(bot, chat_id, user_id, accountant)

        self._menu_markup = menu_markup
        self.payer_ids = []
        self.payee_ids = []
        self._payment_name = None
        self._amounts = None
        self._rates = None
        self._confirmation_string = None
        self._group_id = gid

        states = [State(name='init'),
                  State(name='start'),
                  State(name='request_payers', on_enter=self.say_request_payers, on_exit=self.bye_request_payers),
                  State(name='request_amount', on_enter=self.say_request_amount),
                  State(name='request_payee', on_enter=self.say_request_payee, on_exit=self.bye_request_payee),
                  State(name='confirm', on_enter=self.say_confirm, on_exit=self.bye_confirm),
                  State(name='request_rates', on_enter=self.say_request_rates),
                  State(name='finished'),
                  State(name='only_command', on_enter=self.say_only_command),
                  ]
        transitions = [
            {'trigger': 'request_payment_name', 'source': 'start', 'dest': 'only_command'},
            {'trigger': 'request_names', 'source': 'start', 'dest': 'request_payers'},
            {'trigger': 'request_names', 'source': 'only_command', 'dest': 'request_payers'},
            {'trigger': 'cancel', 'source': 'request_payers', 'dest': 'finished'},
            {'trigger': 'received_names', 'source': 'request_payers', 'dest': 'request_amount'},
            {'trigger': 'received_amounts', 'source': 'request_amount', 'dest': 'request_payee'},
            {'trigger': 'received_names', 'source': 'request_payee', 'dest': 'confirm'},
            {'trigger': 'cancel', 'source': 'request_amount', 'dest': 'finished'},
            {'trigger': 'confirm', 'source': 'request_amount', 'dest': 'confirm'},
            {'trigger': 'confirmed', 'source': 'confirm', 'dest': 'finished'},
            {'trigger': 'rates', 'source': 'confirm', 'dest': 'request_rates'},
            {'trigger': 'confirmed', 'source': 'request_rates', 'dest': 'finished'},
        ]

        self.machine = Machine(model=self, states=states, transitions=transitions, initial='init',
                               ignore_invalid_triggers=True)

        self.responders = {'start': self.respond_start,
                           'only_command': self.respond_only_command,
                           'request_amount': self.respond_request_amount,
                           'confirm': self.respond_confirm,
                           'finished': self.respond_finished,
                           'request_rates': self.respond_request_rates,
                           'request_payers': self.respond_request_payers,
                           'request_payee': self.respond_request_payee,
                           }

        self.query_handlers = {'request_payers': self.query_request_payers,
                               'request_payee': self.query_request_payee,
                               'confirm': self.query_confirm,
                               }

        confirm_markup = t.InlineKeyboardMarkup([[t.InlineKeyboardButton('да', callback_data='да'),
                                                  t.InlineKeyboardButton('нет', callback_data='нет'),
                                                  self.buttons['cancel']]])
        select_markup = t.ReplyKeyboardMarkup([['дальше', 'отменить']], resize_keyboard=True, one_time_keyboard=True)

        self.markups = {'confirm': confirm_markup,
                        'request_payers': select_markup,
                        'request_payee': select_markup,
                        }

        self._reset_state()

    def _reset_state(self):
        self.payer_ids = []
        self._payment_name = None
        self._amounts = None
        self._rates = None
        self._confirmation_string = None
        self.to_start()

    def respond_start(self, text):
        cmd_split = text.split(' ')

        if len(cmd_split) == 0 or text == "":
            self.trigger('request_payment_name')
        elif len(cmd_split) == 2 and cmd_split[0] == '/pay':
            # Начать создание
            self._payment_name = cmd_split[1]
            self.trigger('request_names')

    def say_only_command(self):
        answer = "Название чека?"
        self.send_message(answer)

    def respond_only_command(self, text):
        self._payment_name = text
        self.trigger('request_names')

    def say_request_payers(self):
        self.send_message("Кто платил?")
        self.send_message_savemarkup("выбери имена ↓", reply_markup=self.markup_names())

    def bye_request_payers(self):
        self.remove_last_markup()

    def query_request_payers(self, callback_query):
        member_ids = self.payer_ids[:]
        if callback_query.data == "/all":
            members = self._accountant.get_members_from_group(self._group_id)
            member_ids = [member.id for member in members]
            if set(self.payer_ids) != set(member_ids):
                self.payer_ids = member_ids
                self.edit_msg_names(callback_query, self.payer_ids)
            self.trigger('received_names')

        else:
            member_id = int(callback_query.data)
            if member_id in member_ids:
                member_ids.remove(member_id)
            else:
                member_ids.append(member_id)
            self.payer_ids = member_ids
            self.edit_msg_names(callback_query, self.payer_ids)

    def respond_request_payers(self, text):
        if text == "дальше":
            if len(self.payer_ids) != 0:
                self.trigger('received_names')
            else:
                self.send_message("выбери одно или несколько имен")
        elif text == "отменить":
            self.to_finished()

    def edit_msg_names(self, callback_query, member_ids):
        keyboard_markup = self.markup_names()
        member_names = [self._accountant.get_member_name(member_id=member_id) for member_id in member_ids]
        self._bot.edit_message_text(
            text='\n'.join(member_names) if len(member_names) != 0 else "выбери имена ↓",
            message_id=callback_query.message.message_id,
            chat_id=self._chat_id,
            reply_markup=keyboard_markup)

    def say_request_amount(self):
        text = "Сколько?"
        if len(self.payer_ids) > 1:
            text += " (через пробел)"
        self.send_message(text)

    def respond_request_amount(self, text):
        # text = "сумма1 коэф1 сумма2 коэф2 ..."
        cmd_split = list(filter(None, re.split(' |,', text)))

        # if len(cmd_split) == 1 and cmd_split[0].lower() in ["стоп", "stop", "отмена", "cancel"]:
        # answer = "Ок, заканчиваем"
        # self.trigger('cancel')
        if len(cmd_split) != len(self.payer_ids):
            self.send_message("Ммм.. вас же %d, напиши столько же чисел" % len(self.payer_ids))
        else:
            amounts = []
            try:
                for i in range(len(self.payer_ids)):                    
                    amount = accountant.eval_string_expression(cmd_split[i])
                    if amount:
                        amounts.append(amount)
                    else:
                        self.send_message("Напиши %d чисел через пробел. "
                                          "Не разобрал вот это: '%s'" % 
                                          (len(self.payer_ids),
                                           cmd_split[i]))
                        return
                    
                self._amounts = amounts

                # TODO add amounts to names
                """msg_names = self.last_messages['request_payers']
                
                
                self._bot.edit_message_text(
                    text='\n'.join(self._payers_names),
                    message_id=msg_names.message_id,
                    chat_id=self._chat_id)"""

                self.trigger('received_amounts')

            except ValueError:
                self.send_message("Не разобрал числа, повтори")

    def say_request_payee(self):
        self.send_message("За кого платили?")
        self.send_message_savemarkup("выбери имена ↓", reply_markup=self.markup_names())

    def bye_request_payee(self):
        self.remove_last_markup()

    def query_request_payee(self, callback_query):
        member_ids = self.payee_ids[:]
        if callback_query.data == "/all":
            members = self._accountant.get_members_from_group(self._group_id)
            member_ids = [member.id for member in members]
            if set(self.payee_ids) != set(member_ids):
                self.payee_ids = member_ids
                self.edit_msg_names(callback_query, self.payee_ids)
            self.trigger('received_names')

        else:
            member_id = int(callback_query.data)
            if member_id in member_ids:
                member_ids.remove(member_id)
            else:
                member_ids.append(member_id)
            self.payee_ids = member_ids
            self.edit_msg_names(callback_query, self.payee_ids)


    def respond_request_payee(self, text):
        if text == "дальше":
            if len(self.payee_ids) != 0:
                self.trigger('received_names')
            else:
                self.send_message("выбери одно или несколько имен")
        elif text == "отменить":
            self.to_finished()

    def say_confirm(self):
        # TODO описать чек
        # payers_names = [self._accountant.get_name_by_uid_from_group(self._group_id, uid)
        # for uid in self._payer_uids]
        self._rates = [1 for payee in self.payee_ids]

        if len(self.payee_ids) > 1:
            text = "Поровну?"
            self.send_message_savemarkup(text)
        else:
            self._confirm_try_add_payment("yes")  # Стыдно за это

    def bye_confirm(self):
        self.remove_last_markup()

    def remove_last_markup(self):
        if self.state in self.last_messages.keys():
            request_msg = self.last_messages[self.state]
            self.logger.debug("EDIT markup: message_id=%s" % str(request_msg.message_id))
            self._bot.edit_message_reply_markup(chat_id=self._chat_id,
                                                message_id=request_msg.message_id,
                                                reply_markup=t.InlineKeyboardMarkup([[]]))

    def query_confirm(self, callback_query):
        confirm_reply = callback_query.data.lower()
        self._confirm_try_add_payment(confirm_reply)

    def respond_confirm(self, text):
        confirm_reply = text.lower()
        self._confirm_try_add_payment(confirm_reply)

    def say_request_rates(self):
        self.send_message_savemarkup(self.make_rates_string())

    def respond_request_rates(self, text):
        # text = "коэф1 коэф2 ..."
        cmd_split = list(filter(None, re.split(' |,', text)))

        # if len(cmd_split) == 1 and cmd_split[0].lower() in ["стоп", "stop", "отмена", "cancel"]:
        # answer = "Ок, заканчиваем"
        # self.trigger('cancel')
        if len(cmd_split) != len(self.payee_ids):
            self.send_message("Ммм.. вас же %d, жду столько же чисел" % len(self.payee_ids))
        else:
            rates = []
            try:
                for i in range(len(self.payee_ids)):                                        
                    rate = accountant.eval_string_expression(cmd_split[i])
                    if rate:
                        rates.append(rate)
                    else:
                        self.send_message("Напиши %d чисел через пробел. "
                                          "Не разобрал вот это: '%s'" % 
                                          (len(self.payee_ids),
                                           cmd_split[i]))
                        return
                    
                    # rates.append(rate)
                if set(self._rates) != set(rates):
                    self._rates = rates
                    # self._bot.edit_message_text(text=self.make_rates_string(),
                    # message_id=self.last_messages[self.state].message_id,
                    # chat_id=self._chat_id)

                self._confirm_try_add_payment("да")

            except ValueError:
                self.send_message("Не разобрал число, повтори")

    def make_rates_string(self):
        text = 'Введи суммы через пробел: "1 1 1"\n'
        text += "\n".join(
            "%s: %g" % (self._accountant.get_member_name(member_id=member_id), rate)
            for member_id, rate
            in zip(self.payee_ids, self._rates))
        return text

    def respond_finished(self, text):
        self.send_message("Ошибка в обработке диалога, застрял в состоянии finished")

    def _try_add_payment(self):
        in_first = set(self.payer_ids)
        in_second = set(self.payee_ids)
        in_second_but_not_in_first = in_second - in_first
        participants = self.payer_ids + list(in_second_but_not_in_first)
        amounts = []
        rates = []
        for member_id in participants:
            amounts.append(self._amounts[self.payer_ids.index(member_id)]
                           if member_id in self.payer_ids
                           else 0)
            rates.append(self._rates[self.payee_ids.index(member_id)]
                         if member_id in self.payee_ids
                         else 0)

        transaction = self._accountant.create_payment_for_group(
            self._group_id,
            self._payment_name,
            participants,
            amounts,
            rates
        )

        # self member
        self_member = self._accountant._member(self._user_id, self._group_id)
        if transaction:
            group_name = self._accountant.get_group_name(self._group_id)
            for member_id in participants:
                if member_id == self_member.id:
                    # not notify self
                    continue
                telegram_uid = self._accountant.get_telegram_uid_from_member(member_id)
                if not telegram_uid:
                    # only notify members with user
                    continue

                member_credit = self._accountant.get_member_credit(member_id)
                user_trans_credit = self._accountant.member_trans_credit(transaction, member_id)
                message = "В группе '%s' добавили чек '%s':\n" % (group_name, self._payment_name)
                message += " %s\n" % (
                    self.make_transaction_description()
                ) + "Твой баланс: %s (изменился на %s)" % (
                    self.rounded_string(member_credit),
                    self.rounded_string(user_trans_credit))
                self.send_message(text=message, chat_id=telegram_uid, reply_markup=None)
        return transaction

    def _confirm_try_add_payment(self, confirm_reply):
        if confirm_reply == "да" or confirm_reply == "yes":
            add_payment_result = self._try_add_payment()
            if add_payment_result:
                self.send_message("Отлично, записал", reply_markup=self._menu_markup)
                self.trigger('confirmed')
            else:
                self.send_message("Ошибка при записи чека. Повторить?")
        elif confirm_reply == "нет" or confirm_reply == "no":
            self.trigger('rates')
        elif confirm_reply == "/cancel":
            # TODO back
            pass

    def is_finished(self):
        return self.state == 'finished'

    def is_started(self):
        return self.state != 'init' and not self.is_finished()

    def markup_names(self):
        members = self._accountant.get_members_from_group(self._group_id)
        # Кнопки с именами
        names_column = [[t.InlineKeyboardButton(member.name,
                                                callback_data=str(member.id))]
                        for member in members]
        last_row = [[t.InlineKeyboardButton("выбрать всех", callback_data="/all")]]
        custom_keyboard = names_column + last_row
        keyboard_markup = t.InlineKeyboardMarkup(custom_keyboard)
        return keyboard_markup

    def make_transaction_description(self):
        # Вася 100, петя 150, за А, Б, В (1:2:0.5)
        # 'Вася' заплатил 100 за А, Б, В
        # Вася, Петя, Саша заплатили 100, 200, 300 за А, Б, В (1:2:0.5)
        text = ""
        payer_names = [self._accountant.get_member_name(member_id=member_id)
                       for member_id in self.payer_ids]
        payee_names = [self._accountant.get_member_name(member_id=member_id)
                       for member_id in self.payee_ids]

        if len(payer_names) == 1:
            text += "'%s' заплатил " % payer_names[0]
        else:
            text += ', '.join(payer_names) + ' заплатили '

        #100, 200, 300 за
        text += ', '.join([self.rounded_string(am) for am in self._amounts]) + ' за '
        text += ', '.join(payee_names)
        if any([x != self._rates[0] for x in self._rates]):
            # Есть разные коэффициенты
            text += " (" + ":".join(["%g" % rate for rate in self._rates]) + ")"
        return text
