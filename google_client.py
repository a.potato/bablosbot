# coding=utf-8

import gspread
from oauth2client.service_account import ServiceAccountCredentials
import time


class GsheetsController(object):
    def __init__(self):
        scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        credentials = ServiceAccountCredentials.from_json_keyfile_name('bablosbot_service_account.json', scope)
        self._client = gspread.authorize(credentials)
        self._spreadsheet = None
        self._wsheets = dict()

    def load(self, name):
        self.relogin()
        self._spreadsheet = self._client.open(name)
        wsheets = self._spreadsheet.worksheets()
        self._wsheets = {wsheet.title: wsheet for wsheet in wsheets}

    def create_spreadsheet(self, name):
        self.relogin()
        self._spreadsheet = self._client.create(name)
        self._wsheets = dict()

    def create_worksheet(self, name):
        self.relogin()
        new_wsheet = self._spreadsheet.add_worksheet(name, rows=200, cols=20)
        self._wsheets[name] = new_wsheet

    def names(self):
        return list(self._wsheets.keys())

    def read_group(self, name):
        self.relogin()
        wsheet = self._wsheets[name]
        values = wsheet.get_all_values()
        return values

    def write_row(self, name, row_num, row_values):
        self.relogin()
        sheet = self._wsheets[name]
        cells = sheet.range(row_num + 1, 1, row_num + 1, len(row_values))
        for i in range(len(cells)):
            cells[i].value = row_values[i]
        sheet.update_cells(cells)

    def delete_row(self, name, row_num):
        self.relogin()
        sheet = self._wsheets[name]
        sheet.delete_row(row_num + 1)

    def write_column(self, name, col_num, col_values):
        self.relogin()
        sheet = self._wsheets[name]
        cells = sheet.range(1, col_num + 1, len(col_values), col_num + 1)
        for i in range(len(cells)):
            cells[i].value = col_values[i]
        sheet.update_cells(cells)

    def read_col(self, name, col_num):
        self.relogin()
        wsheets = self._wsheets[name]
        col_cells = wsheets.range(1, col_num + 1, wsheets.row_count, col_num + 1)
        # last not-empty cell index
        last_i = 0
        for i in reversed(range(len(col_cells))):
            if col_cells[i].value == "":
                last_i = i
            else:
                break
        return [cell.value for cell in col_cells[:last_i]]

    def has_name(self, dbname):
        return dbname in self._wsheets.keys()
        pass

    def delete_wsheet(self, name):
        return
        self.relogin()
        self._client.del_worksheet(self._wsheets[name])

    def relogin(self):
        self._client.login()