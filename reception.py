#!/usr/bin/python
# coding=utf-8
import re

import telegram
from dialog_front import DialogFront
import traceback
from data.data_model import User
from data import data_model as dm

class Reception(object):
    def __init__(self, bot, accountant, log, db_session):
        self.bot = bot
        self.accountant = accountant
        self.users = []
        self.dialogs = dict()
        self.db_userlist = "userlist"
        self.log = log
        self._db = db_session

    def save_userlist(self):
        if not self.gcontroller.has_name(self.db_userlist):
            self.gcontroller.create_worksheet(self.db_userlist)
        self.gcontroller.write_column(self.db_userlist, 0, [str(user) for user in self.users])

    def check_add_user(self, user_id):
        is_new = False

        # Количество пользователей с таким UID
        search_count = self._db.query(dm.User).filter_by(telegram_uid=user_id).count()
        if search_count == 0:
            is_new = True
            # Если 0, добавлен пользователь
            self.log("new user: %d" % user_id)
            user = dm.User(telegram_uid=user_id)
            self._db.add(user)
            try:
                self.bot.send_message(chat_id=100806533, text="New user: %d" % user_id)
            except telegram.error.NetworkError as ex:
                self.log("exception while sending msg to owner: " + str(ex))
        else:
            assert search_count == 1, "user_id found multiple times: %s" % user_id

        return is_new

    def handle_start(self, user_id, text):
        is_new = self.check_add_user(user_id)

        # Прочитать параметр, если это deeplink '/start parameter'
        parameter = None
        if text[:7] == '/start ':
            parameter = text[7:]

        # Принудительно создать новый диалог
        try:
            self.dialogs[user_id] = DialogFront(self.bot, user_id, user_id, self.accountant, parameter)
        except Exception as ex:
            self.log(traceback.format_exc())

        # if not is_new:
        #     try:
        #         self.dialogs[user_id].process(update.message.text)
        #     except Exception as ex:
        #         self.log(traceback.format_exc())

    def handle_echo(self, update):
        text = update.message.text
        # Заменить децимальную , на .
        comma_num = re.sub(r'(\d+),(\d+)', r'\1.\2', text)
        while comma_num != text:
            text = comma_num
            comma_num = re.sub(r'(\d+),(\d+)', r'\1.\2', text) # 213,12312 -> 213.12312

        if re.fullmatch(r'[\d,.\s]+', text):
            text = text.replace(',', '.')

        user_id = update.message.from_user.id
        is_new = self.check_add_user(user_id)

        if is_new:
            self.log("echo from unknown user; adding %s" % user_id)

        # Создать диалог, если его не было
        if user_id not in self.dialogs.keys():
            self.dialogs[user_id] = DialogFront(self.bot, user_id, user_id, self.accountant)
        else:
            try:
                self.dialogs[user_id].process(text)
            except Exception as ex:
                self.log(traceback.format_exc())

    def handle_query(self, update):
        user_id = update.callback_query.from_user.id
        self.bot.answer_callback_query(callback_query_id=update.callback_query.id)

        if self.check_add_user(user_id):
            self.log("query from unknown user; adding %s" % user_id)
        else:
            try:
                self.dialogs[user_id].handle_query(update.callback_query)
            except Exception as ex:
                self.log("exception in handling query in dialog %s: %s" %
                         (self.dialogs[user_id].__class__.__name__, traceback.format_exc()))
        pass
