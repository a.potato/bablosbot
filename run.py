#!/usr/bin/python
# coding=utf-8

from bot import Bot
from data import data_model

if __name__ == '__main__':
    print('starting bot')
    try:
        bot = Bot()
        bot.work()
    except Exception as ex:
        bot.close()
        print("i'm dead, bye")
        raise ex

    #s = data.create_session()
    #u = data.User(telegram_uid=213)
    #s.add(u)