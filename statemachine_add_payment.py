#!/usr/bin/python
# coding=utf-8

import statemachine

class AddPaymentMachine(statemachine.Machine):
    initial_state = 'requested'

    @statemachine.event
    def cycle(self):
        yield