import unittest
import accountant
import data.data_model as dm


class CreatePaymentForGroup(unittest.TestCase):
    def test_something(self):
        _db_session = dm.create_session('sqlite:///../database_new.db')
        acc = accountant.Accountant(log=print,
                                    db_session=_db_session)

        transaction = acc.create_payment_for_group(
            group_id=1,
            payment_name="New payment",
            #member_ids=[182116723, 100806533, 410903251],
            member_ids=[1, 2, 3],
            amounts=[11, 22, 33],
            rates=[0.3, 0.4, 0.5]
        )
        _db_session.close()

        self.assertTrue(transaction)


if __name__ == '__main__':
    unittest.main()
