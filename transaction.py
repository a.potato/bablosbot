

class Payment(object):
    def __init__(self, amount, rate):
        self.amount = amount
        self.rate = rate

class Transaction(object):
    trans_id = 0

    def __init__(self, name, trans_dict):
        self.name = name
        self.payments_dict = trans_dict
        self.id = Transaction.trans_id
        Transaction.trans_id += 1

    def has_user(self, uid):
        return uid in self.payments_dict.keys()

    def get_credit_for(self, uid):
        if uid not in self.payments_dict.keys():
            raise Exception("uid not found in transaction")

        trans_sum = 0.0
        rate_sum = 0.0
        for payer_id, payment in self.payments_dict.items():
            trans_sum += payment.amount
            rate_sum += payment.rate

        if rate_sum == 0:
            return 0
        else:
            amount_by_rate = trans_sum / rate_sum
            credit = self.payments_dict[uid].amount - amount_by_rate * self.payments_dict[uid].rate
            return credit
